// 保存用户相关信息的模块
import { login,getUserInfo } from '@/api/user'
import { setToken, removeToken, getToken } from '@/utils/auth'

export default {
  namespaced: true,
  state: {
    token: getToken(),
    userInfo: {}
   
  },
  mutations: {
    setToken(state, token) {
      state.token = token
      setToken(token)
    },
    setUserInfo(state, data) {
        state.userInfo = data
       
    },
    // 移除token
    removeToken(state) {
      state.token = null
      removeToken()
    },
},
actions: {
    async getlogin(store, data) {
      const res = await login(data)
      store.commit('setToken', res.data.jwt)
      console.log(res)
      // 保存时间戳
      localStorage.setItem('roomTime', Date.now()) 
    },
    async getUserInfo(store){
        const res = await getUserInfo()
        console.log(36,res.data);
        store.commit('setUserInfo',res.data)
    },
    logout(store) {
      // 删除token---本地存储
      store.commit('removeToken')
      // 删除token---user
      store.commit('removeUserInfo')
    }
  }
}
