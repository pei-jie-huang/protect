import axios from 'axios'
// import { Message, Loading } from 'element-ui'
import store from '@/store'
import { Loading, Message } from "element-ui";


import  router  from '@/router/index';
// import router from '@/router'
// create an axios instance
let loading;
const request = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 50000 // 请求超时时间
})

// 设置token有效时长
// const time = 24 * 60 * 60 * 1000 // 有效时长1天
// let loading
// 请求拦截器
request.interceptors.request.use(config => {
  // loading = Loading.request({
  //   lock: true,
  //   text: "Loading",
  //   spinner: "el-icon-loading",
  //   background: "rgba(0,0,0,0.7)",
  // });
  const token = store.state.user.token
  const whiteArr = ['/auth/local/register', '/auth/local']
  if (token && !whiteArr.includes(config.url)) {
    config.headers.Authorization = 'Bearer ' + token
  }
  return config // 放行
})

// 响应拦截器
request.interceptors.response.use(
  (res)=>{
    // loading.close(); //关闭loading效果
    console.log(28,'网络层面成功');
    console.log(res,29);
    console.log(30,res.config);
    console.log(31,res.config.baseURL);
    console.log(32,res.config.url);
    // console.log(42,$router.path);
    // function debounce(func,wait){
    //   let timeout
    //   return function(){
    //     let context = this
    //     let args =arguments
    //     clearTimeout(timeout)
    //     timeout = setTimeout(function(){
    //       func.apply(context,args)
    //     },wait)
    //   }
    // }
    let path = router.currentRoute.fullPath
    let url = res.config.baseURL
    console.log(path,57)
      if(url+'/auth/local/register'&&path=="/login?redirect=%2Fdashboard"){
        Message.success('恭喜您注册成功！');
        return res
      }else if(url+'/users/me'&&path=="/dashboard"){
        Message.success('恭喜您登录成功！');
        return res
      }
  
    return res
    // }// fn 是需要执行的函数
// wait 是时间间隔
// const throttle = (fn, wait = 50) => {
//   // 上一次执行 fn 的时间
//   let previous = 0
//   // 将 throttle 处理结果当作函数返回
//   return function(...args) {
//     // 获取当前时间，转换成时间戳，单位毫秒
//     let now = +new Date()
//     // 将当前时间和上一次执行函数的时间进行对比
//     // 大于等待时间就把 previous 设置为当前时间并执行函数 fn
//     if (now - previous > wait) {
//       previous = now
//       fn.apply(this, args)
//     }
//   }
// }
// DEMO
// 执行 throttle 函数返回新函数
// const betterFn = throttle(() => console.log('fn 函数执行了'), 1000)
// 每 10 毫秒执行一次 betterFn 函数，但是只有时间差大于 1000 时才会执行 fn
  
  }
  // ,err=>{
  //   loading.close(); //关闭loading效果
  //   console.log("网络层面失败", err);
  //   console.dir(err); // 可以看到更详细的错误信息
  // }
  
)

export default request
