import request from '@/utils/request'

// 登录
export function login(data) {
  return request({
    url: '/auth/local',
    method: 'post',
    data
  })
}
// 注册
export function register(data) {
  return request({
    url: '/auth/local/register',
    method: 'post',
    data
  })
}
// 获取用户接口
export function getUserInfo() {
  return request.get('/users/me')
}
