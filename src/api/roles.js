// 封装角色管理的接口
import request from '@/utils/request'

// 获取角色列表
export function getRoleList(params) {
  return request({
    url: '/roles',
    params
  })
}

// 获取角色数量
export function getRoleCount() {
  return request({
    url: '/roles/count'
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/roles',
    method: 'post',
    data
  })
}

// 删除角色
export function delRole(id) {
  return request({
    url: `/roles/${id}`,
    method: 'delete'
  })
}

// 根据id获取角色详情
export function getRoleDetail(id) {
  return request({
    url: `/roles/${id}`
  })
}

// 编辑角色
export function editRole(data) {
  return request({
    url: `/roles/${data.id}`,
    method: 'put',
    data
  })
}

// 获取权限列表
export function getPermissiosnList() {
  return request({
    url: '/permissions'
  })
}

// 获取当前用户权限
export function getCurrentPermission() {
  return request({
    url: '/mypermissions'
  })
}

// 根据id获取指定权限
export function getSpecifyPermission(id) {
  return request({
    url: '/permissions/:' + id
  })
}

