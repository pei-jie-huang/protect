import request from '@/utils/request'

// 获取客户列表
export function getClient(params) {
  return request.get('/clients', { params })
}

// 新建客户
export function getClientAdd(data) {
  return request.post('/clients', data)
}

// 获取指定id
export function getClientID(id) {
  return request.get(`/clients/${id}`)
}

// 编辑客户
export function getClientPut(data) {
  return request.put(`/clients/${data.id}`, data)
}

// 删除客户
export function getClientDel(id) {
  return request.delete(`/clients/${id}`)
}
