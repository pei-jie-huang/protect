import request from '@/utils/request'
// 获取经纪列表
export function getProperty(data) {
  return request.get('/properties', { data })
}
// 获取房产列表
export function getPropetyFormList() {
  return request({
    url: '/properties'
  })
}

// 新增房产
export function addPropetyForm(data) {
  return request({
    url: '/properties',
    method: 'post',
    data
  })
}

