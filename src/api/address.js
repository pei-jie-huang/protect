import axios from 'axios'

let geoData = null

// 获取地址-省市县
export function getAddress() {
  if (geoData) {
    console.log('有数据，直接返回')
    return geoData
  } else {
    console.log('没数据，需要请求')
    geoData = axios({
      // 接口地址
      url: 'https://restapi.amap.com/v3/config/district',
      method: 'get',
      params: {
        // 高德开放平台账户key值
        key: 'b79082c628a57c4331f2ab4001ea2341',
        // 返回下级 3代表省市县三级
        subdistrict: 3
      }
    })
    return geoData
  }
}
