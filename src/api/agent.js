import request from '@/utils/request'
// 获取经纪列表
export function getAgents(params) {
  return request.get('/agents', { params })
}
// 新增经纪
export function addAgents(data) {
  return request.post('/agents', data)
}
// 删除面经
export function delAgent(id) {
  return request.delete(`/agents/${id}`)
}
// 获取指定面经
export function getAgentById(id) {
  return request.get(`/agents/${id}`)
}
// 编辑面经
export function updateAgent(data) {
  return request.put(`/agents/${data.id}`, data)
}
// 获取经纪数量
export function getAgentCount(params) {
  return request.get('/agents/count', { params })
}
