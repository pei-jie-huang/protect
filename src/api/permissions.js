// 权限管理
import request from '@/utils/request'
// 获取权限列表
export function getPermissList() {
  return request.get('/permissions')
}
// 添加权限功能
export function addPermissList(data) {
  return request.post('/permissions', data)
}
// 删除权限
export function delPermissList(id) {
  return request.delete('/permissions/' + id)
}
// 获取指定权限
export function editPerm(id) {
  return request.get('/permissions/' + id)
}
// 获取编辑接口
export function alterPerm(data) {
  return request.put('/permissions/' + data.id, data)
}
