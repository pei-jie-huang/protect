// 页面管理
import request from '@/utils/request'

// 获取页面列表
export function getPageList(params) {
  return request.get('/pages', { params })
}
// 获取页面页数
export function getPageLimit(params) {
  return request.get('/pages/count', { params })
}
// 新增页面
export function addPageList(data) {
  return request.post('/pages', data)
}

// 删除页面
export function delPageList(id) {
  return request.delete('/pages/' + id)
}
// 获取单个列表
export function compileList(id) {
  return request.get('/pages/' + id)
}
// 编辑列表
export function alterPage(data) {
  return request.put('/pages/' + data.id, data)
}
