// 封装分类管理相关的接口
import request from '@/utils/request'

// 获取分类列表
export function getCategorieList(params) {
  return request({
    url: '/categories',
    params
  })
}

// 获取分类数量
export function getCategorieCount(title_contains) {
  return request({
    url: '/categories/count',
    params: {
      title_contains
    }
  })
}

// 新增分类
export function addCategories(data) {
  return request({
    url: '/categories',
    method: 'post',
    data
  })
}

// 删除分类
export function delCategories(id) {
  return request({
    url: `/categories/${id}`,
    method: 'delete'
  })
}

// 获取指定分类
export function getCategorieSpecify(id) {
  return request({
    url: `/categories/${id}`
  })
}

// 编辑分类
export function editCategories(data) {
  return request({
    url: `/categories/${data.id}`,
    method: 'put',
    data
  })
}

//
