import request from '@/utils/request'

// 获取员工列表
export function getEmployeesList(params) {
  return request.get('/users', { params })
}
// 用户数量
export function userCount() {
  return request.get('/users/count')
}
// 删除用户
export function delEmployee(id) {
  return request.delete(`/users/${id}`)
}
// 根据id获取用户数据
export function getEmployeeById(id) {
  return request.get(`/users/${id}`)
}
// 编辑用户
export function updateEmployee(data) {
  return request.put(`/users/${data.id}`, data)
}
