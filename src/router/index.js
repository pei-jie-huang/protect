import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
// 引入动态路由
import pages from './modules/pages'
import agents from './modules/agents'
import categories from './modules/categories'
import client from './modules/client'
import permissions from './modules/permissions'
import property from './modules/property'
import roles from './modules/roles'
import employees from './modules/employees'

export const asyncRoutes = [categories, pages, agents, client, property, permissions, roles, employees]

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/', // 配置访问路径的书写
    component: Layout, // 注册对应的组件
    redirect: '/dashboard', // 重定向…强制跳转到/dashboard这个路径上
    children: [{ // 子路由
        path: 'dashboard',
        name: 'dashboard', // 路由名称
      component: () => import('@/views/dashboard/index'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '首页', // 导航标题
        icon: 'dashboard' // 标题旁边图标
      }
    }]
  },
  // 404 page must be placed at the end !!!
  { path: '/404', component: () => import("@/views/404"), hidden: true },
  { path: '*', redirect: "/404", hidden: true },
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes, ...asyncRoutes]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
