import Layout from '@/layout'

export default {
  path: '/pages',
  component: Layout,
  name: 'pages',
  children: [
    {
      path: '',
      component: () => import('@/views/pages/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '页面管理', // 导航标题
        icon: 'el-icon-ice-drink' // 标题旁边图标
      }
    }
  ]

}
