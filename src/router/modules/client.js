import Layout from '@/layout'

export default {
  path: '/client',
  component: Layout,
  name: 'client',
  children: [
    {
      path: '',
      component: () => import('@/views/client/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '客户管理', // 导航标题
        icon: 'el-icon-user-solid' // 标题旁边图标
      }
    }
  ]

}
