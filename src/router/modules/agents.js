import Layout from '@/layout'

export default {
  path: '/agents',
  component: Layout,
  name: 'agents',
  children: [
    {
      path: '',
      component: () => import('@/views/agents/index'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '经纪管理', // 导航标题
        icon: 'el-icon-news' // 标题旁边图标
      }
    }
  ]

}
