import Layout from '@/layout'

export default {
  path: '/employees',
  component: Layout,
  name: 'employees',
  children: [
    {
      path: '',
      component: () => import('@/views/employees/index'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '员工管理', // 导航标题
        icon: 'el-icon-suitcase-1' // 标题旁边图标
      }
    }
  ]

}
