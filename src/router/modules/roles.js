import Layout from '@/layout'

export default {
  path: '/roles',
  component: Layout,
  name: 'roles',
  children: [
    {
      path: '',
      component: () => import('@/views/roles/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '角色管理', // 导航标题
        icon: 'el-icon-paperclip' // 标题旁边图标
      }
    }
  ]

}
