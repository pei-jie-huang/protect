import Layout from '@/layout'

export default {
  path: '/permissions',
  component: Layout,
  name: 'permissions',
  children: [
    {
      path: '',
      component: () => import('@/views/permissions/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '权限管理', // 导航标题
        icon: 'el-icon-s-data' // 标题旁边图标
      }
    }
  ]

}
