import Layout from '@/layout'

export default {
  path: '/categories',
  component: Layout,
  name: 'categories',
  children: [
    {
      path: '',
      component: () => import('@/views/categories/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '分类管理', // 导航标题
        icon: 'el-icon-connection' // 标题旁边图标
      }
    }
  ]

}
