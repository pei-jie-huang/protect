// import Layout from '@/layout'
// 客户管理路由
export const clientModule = {
  children: [
    {
      path: '',
      name: 'Client',
      component: () => import('@/views/clientModule/index.vue'),
      meta: { title: '客户管理', icon: 'form' }
    }
  ]
}
