import Layout from '@/layout'

export default {
  path: '/property',
  component: Layout,
  name: 'property',
  children: [
    {
      path: '',
      component: () => import('@/views/property/index.vue'),
      meta: { // 路由元信息--作用:就是用来保存数据的
        title: '房产管理', // 导航标题
        icon: 'el-icon-s-home' // 标题旁边图标
      }
    }
  ]

}
