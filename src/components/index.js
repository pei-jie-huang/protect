// 全局注册组件

import ImageUpload from './ImageUpload/index.vue'
import Vectorgraph from './Vectorgraph/vectorgraph.vue'
import pinkOcean from './pinkOcean/pinkOcean.vue'
import login from './login/login.vue'

export default {
  install(Vue) {
    Vue.component('ImageUpload', ImageUpload)
    Vue.component('Vectorgraph', Vectorgraph)
    Vue.component('pinkOcean', pinkOcean)
    Vue.component('login', login)
    Vue.component('Vectorgraph', Vectorgraph)
    Vue.component('pinkOcean', pinkOcean)
    Vue.component('login', login)
  }
}
