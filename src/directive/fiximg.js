// 设置默认头像值
// import fiximg from '@/directive/fiximg'
// Vue.directive('imgerror', fiximg)
export default {
  inserted(el, binding) {
    el.addEventListener('error', function() {
      el.src = binding.value
    })
    //   判断src是否为空
    el.src = el.src || binding.value
  }

}
