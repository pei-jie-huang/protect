
// import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
// import getPageTitle from '@/utils/get-page-title'
import router from '@/router/index'
import store from '@/store/index'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

// 路由守卫
router.beforeEach(async(to, from, next) => {
  // to:跳转的目标路由对象
  // from：当前路由对象
  // next：放行函数
  console.log(store.state.user.token, 'token20')
  console.log(21, store.state)
  console.log(22,store.state.userInfo)
  if (store.getters.token) {
    // 登录了
    // 登录成功后不允许用户访问登录页面--直接跳转到主页即可
    if (to.path === '/login') {
      next('/')
    } else {
        if (!store.state.user.userInfo.id) {
          await store.dispatch('user/getUserInfo')
        }
    next()
    }
  } else {
    // 未登陆
    // 当用户访问的页面就是登陆页面的时候 应该直接放行
    // 设置白名单
    const whiteArr = ['/login', '/404']
    // if (to.path === '/login' || to.path === '/404') {
    if (whiteArr.includes(to.path)) {
      next()
    } else {
      next('/login') // 当我们通过next跳转到某个页面的时候，会重新触发导航守卫
      // 跳转回登陆页面
    }
  }
  next()
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
